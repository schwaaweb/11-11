// This loads all of the stuff here
// https://docs.rs/nannou/0.15.0/nannou/prelude/index.html
use nannou::prelude::*;

extern crate csound;
use csound::Csound;

/* Defining our Csound ORC code within a multiline String */
static ORC: &str = "sr=48000
  ksmps=32
  nchnls=2
  0dbfs=1
  instr 1
  aout vco2 0.5, 390
  outs aout, aout
endin";

/*Defining our Csound SCO code */
static SCO: &str = "i1 0 1";

fn main() {
    // program begins here and ends when main() ends
    nannou::app(model) // start building the app  and specify the 'model'
        .update(update) // handle events with 'event'
        .simple_window(view) // use a 'view' to draw in a simple window
        .run(); // hit it Run!

    let cs = Csound::new();

    /* Using SetOption() to configure Csound
    Note: use only one commandline flag at a time */
    cs.set_option("-odac").unwrap();

    /* Compile the Csound ORChestra string */
    cs.compile_orc(ORC).unwrap();

    /* Compile the Csound SCO String */
    cs.read_score(SCO).unwrap();

    /* When compiling from strings, this call is necessary
     * before doing any performing */
    cs.start().unwrap();

    /* Run Csound to completion */
    cs.perform();

    cs.stop();
}

// we define the state of the application here
struct Model {}

// run once to setup the 'model'
fn model(_app: &App) -> Model {
    Model {}
}

// the c ontroller in mvc model, view, controller
fn update(_app: &App, _model: &mut Model, _update: Update) {}

// view
fn view(_app: &App, _model: &Model, frame: Frame) {
    frame.clear(ORANGE);
}
